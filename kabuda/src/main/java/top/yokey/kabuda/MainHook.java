package top.yokey.kabuda;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class MainHook implements IXposedHookLoadPackage {

    public MainHook() {
        super();
    }

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam param) {

        if (param.packageName.contains("com.taobao.litetao")) {

            XposedHelpers.findAndHookMethod(XposedHelpers.findClassIfExists("mtopsdk.mtop.global.e", param.classLoader), "e", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    param.setResult(false);
                }
            });
            XposedHelpers.findAndHookMethod(XposedHelpers.findClassIfExists("mtopsdk.mtop.global.e", param.classLoader), "d", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    param.setResult(false);
                }
            });

        }

    }

}
